import React from 'react';
import {StyleSheet, View, Text} from 'react-native';

export default class App extends React.Component {
    render(){
        return(
            <View style={styles.container}>
                <Text>Ini Tugas 12</Text>
            </View>
        )
    }        
}

const styles= StyleSheet.create({
    container : {
        flex:1,
        alignItems: 'center',
        justifyContent: 'center'
    }
});