import React from 'react'; 
import { View, 
         Platform, 
         Text, 
         Image, 
         TextInput, 
         TouchableOpacity, 
         StyleSheet, 
         Button,
         KeyboardAvoidingView, 
         ScrollView } from 'react-native';


const AboutScreen = () => {
    return(
        <ScrollView>
            <View style={styles.container}>
                <Text style={styles.judul}>Tentang Saya</Text>
                <FontAwesome5 
                    name="user-circle"
                    size={200} solid
                    color="#EFEFEF"
                    style={styles.icon}
                />
                <Text style={styles.nama}>Efan</Text>
                <Text style={styles.kerjaan}>Tukang Ketik</Text>
                <View style={styles.kotak}>
                    <Text style={styles.juduldalam}>Portofolio</Text>
                    <View style={styles.kotakdalam}>
                        <View>
                            <FontAwesome5 name="gitlab" size={40} style={styles.icon}/>
                            <Text style={styles.textdalam}>@efan_</Text>
                        </View>
                        <View>
                            <FontAwesome5 name="github" size={40} style={styles.icon}/>
                            <Text style={styles.textdalam}>@efan_</Text>
                        </View>
                        
                    </View>
                </View>
                <View style={styles.kotak}>
                    <Text style={styles.juduldalam}>Hubungi Saya</Text>
                    <View style={styles.kotakdalamver}>
                        <View style={styles.kotakdalamverhub}>
                            <View>
                                <FontAwesome5 name="facebook" size={40} style={styles.icon}/>  
                                <Text style={styles.textdalam}>@efan_</Text>
                            </View>
                        </View>
                        <View style={styles.kotakdalamverhub}>
                            <View>
                                <FontAwesome5 name="instagram" size={40} style={styles.icon}/>    
                                <Text style={styles.textdalam}>@efan_</Text>
                            </View>
                        </View>
                        <View style={styles.kotakdalamverhub}>
                            <View>
                                <FontAwesome5 name="twitter" size={40} style={styles.icon}/>    
                                <Text style={styles.textdalam}>@efan_</Text>
                            </View>
                            <View>
                                <Text style={styles.textdalam}>@efan_</Text>
                            </View>
                        </View>
                        

                    </View>
                </View>
            </View>
        </ScrollView>
    );
}

export default AboutScreen

const styles = StyleSheet.create({
    container:{
        marginTop:64
    },
    judul:{
        fontSize:36,
        fontWeight:"bold",
        color: '#003366',
        textAlign:'center'
    },
    icon:{
        textAlign:'center'
    },
    nama:{
        fontSize:24,
        fontWeight:"bold",
        color:'#003366',
        textAlign:'center'
    },
    kerjaan:{
        fontSize:16,
        fontWeight:"bold",
        color: '#3EC6FF',
        textAlign:'center',
        marginBottom:7
    }
});
