import React from 'react'; 
import { View, 
         Platform, 
         Text, 
         Image, 
         TextInput, 
         TouchableOpacity, 
         StyleSheet, 
         Button,
         KeyboardAvoidingView, 
         ScrollView } from 'react-native';

const LoginScreen = () =>{
    return(
        <KeyboardAvoidingView
            behavior = {Platform.OS=="ios" ? "padding" : "height"}
            style = {Styles.container}
        >

        <ScrollView>
            <View style={Styles.containerView}>
                <Image source={require('../Tugas13/assets/logo.png')} />
                <Text style={styles.logintext}>Login</Text>
                <View style={styles.formInput}>
                    <Text style={styles.formText}>Username</Text>
                    <TextInput style={styles.input}/>
                </View>
                <View style={styles.formInput}>
                    <Text style={styles.formText}>Password</Text>
                    <TextInput style={styles.input} secureTextEntry={true}/>
                </View>
                <View style={styles.kotaklogin}>
                    <TouchableOpacity style={styles.btlogin}>
                        <Text style={styles.textbt}>Masuk</Text>
                    </TouchableOpacity>
                    <Text style={styles.autoText}>atau</Text>
                    <TouchableOpacity style={styles.btreg}>
                        <Text style={styles.textbt}>Daftar</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </ScrollView>
        </KeyboardAvoidingView>
    )
}

const styles = StyleSheet.create({
    container:{
        flex:1,

    },
    logintext:{
        fontSize:24,
        marginTop:63,
        textAlign:'center',
        color: '#003366',
        marginVertical:20 
    },
    formText:{
        color:'#003366'
    },
    autoText:{
        fontSize:20,
        color: '#3EC6FF',
        allignItems:'center',
        padding:10,
        marginHorizontal:30,
        marginBottom:10,
        width:140,
    },
    formInput:{
        marginHorizontal:30,
        marginVertical: 5,
        allignContent:'center',
        width: 294
    },
    input:{
        height:40,
        borderColor: '#003366',
        padding:10,
        borderWidth:1
    },
    btlogin:{
        allignItems:'center',
        backgroundColor: '#3EC6FF',
        padding:10,
        borderRadius:16,
        marginHorizontal:30,
        marginBottom:10,
        width:140,
    },
    btreg:{
        allignItems:'center',
        backgroundColor: '#003366',
        padding:10,
        borderRadius:16,
        marginHorizontal:30,
        marginBottom:10,
        width:140,
    }

});